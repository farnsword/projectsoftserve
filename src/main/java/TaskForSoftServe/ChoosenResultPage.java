package TaskForSoftServe;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChoosenResultPage {
    private WebDriver driver;
    @FindBy (css = "div.pet_number + h1")
    private WebElement petitionName;

    public ChoosenResultPage(){
        driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
    }

    public ChoosenResultPage(WebDriver someDriver){
        if(someDriver == null){
            this.driver = new ChromeDriver();
        } else {
            this.driver = someDriver;
        }
        PageFactory.initElements(driver, this);
    }

    public String getPetitionName(){
        return petitionName.getText();
    }


}
