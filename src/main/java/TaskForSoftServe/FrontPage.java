package TaskForSoftServe;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrontPage {
    private WebDriver driver;
    private String searchText= "РОБОЧИЙ ЧАС — УКРАЇНЦЯМ!";
    private By searchField = By.xpath(".//input[@name=\"text\"]");
    private By searchButton = By.cssSelector("i.fa.fa-search");

    public FrontPage(){
        if(driver == null){
            this.driver = new ChromeDriver();
        }
    }

    public FrontPage(WebDriver someDriver){
        if(someDriver == null){
            this.driver = new ChromeDriver();
        } else {
            this.driver = someDriver;
        }
    }

    public void fillSearchField(){
        driver.findElement(searchField).sendKeys(searchText);
    }

    public void clickSearchButton(){
        driver.findElement(searchButton).click();
    }
}
