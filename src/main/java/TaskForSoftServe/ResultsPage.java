package TaskForSoftServe;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class ResultsPage {
    private WebDriver driver;
    private By checkResultsElement = By.cssSelector("div.page_result div.list_elem_col");
    private By result = By.xpath(".//a[contains(., \"РОБОЧИЙ\")]");

    public ResultsPage() {
        if (driver == null) {
            driver = new ChromeDriver();
        }
    }

    public ResultsPage(WebDriver someDriver) {
        if (someDriver == null) {
            this.driver = new ChromeDriver();
        } else {
            this.driver = someDriver;
        }
    }

    public boolean checkElement() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        boolean res = driver.findElements(checkResultsElement).size() > 0;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return res;
        }
    

    public void chooseResult() {
        driver.findElement(result).click();
    }
}
