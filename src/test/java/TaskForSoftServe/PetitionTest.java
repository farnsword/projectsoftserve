package TaskForSoftServe;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PetitionTest {
    static WebDriver driver;
    final static String URL_LINK = "https://petition.president.gov.ua";

    @BeforeTest
    public void preConditionI(){
        System.setProperty("webdriver.chrome.driver", "/home/oleksii/IdeaProjects/chromedriver");
        driver = new ChromeDriver();
        driver.get(URL_LINK);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void petitionTest(){
        FrontPage mainPage = new FrontPage(driver);
        mainPage.fillSearchField();
        mainPage.clickSearchButton();

        ResultsPage results = new ResultsPage(driver);
        Assert.assertFalse(results.checkElement());
        results.chooseResult();

        ChoosenResultPage petition = new ChoosenResultPage(driver);
        WebElement wait = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//a[@data-id=\"pet-tab-1\"]")));
        Assert.assertEquals(petition.getPetitionName(), "СКОРОЧЕНИЙ європейський РОБОЧИЙ ЧАС — УКРАЇНЦЯМ!");
    }

    @AfterTest
    public void postCondition(){
        driver.close();
    }

}
